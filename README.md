# gosim

## Install

Install go.  If on windows, follow [build pixel on windows](https://github.com/faiface/pixel/wiki/Building-Pixel-on-Windows)
then, 
```
go get -u gitlab.com/fengttt/gosim/...
```

## Examples
* Conway's Game of Life
* A simple infection disease model


